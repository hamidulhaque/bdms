<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
    #log{
  background-image: url("log.png");
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

    padding-top: 40px;          
    align-items: center;
	margin-left: 40px;
	
	padding: 10px;
	box-shadow: 2px 8px 16px 0px rgba(0, 0, 0, 0.2);
	border-radius: 25px;
	scroll-behavior: none;
	padding: 20px;}
    
.btn{
 position: relative;
  display: inline-block;
  font-size: 3em;
  font-weight: 800;
  color: royalblue;
  overflow: hidden;
  background: linear-gradient(to right, blue,blue 50%, lightgray 50%);
  background-clip: text;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  background-size: 200% 100%;
  background-position: 100%;
  transition: background-position 300ms ease;
  text-decoration: none; 
 
}
    .btn:hover{
        background-position: 0 100%;
    }



</style>
</head>
<body>
	<header class="header">
	<nav class="navbar navbar-style">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#micon">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>	
				</button>


				<a href="main.php" ><img class="logo" src="logo.png" alt=""></img></a>
				<a href="main.php" >bdms</a>

			</div>

			<div class="collapse navbar-collapse" id="micon">
				<ul class="nav navbar-nav navbar-right">
				<li class="nav-item">
					<a href="main.php">Home</a>
				</li>
				<li class="nav-item">
					<a href="gallary.php">Gallery</a>
				</li>
				<li class="nav-item">
					<a href="events.php">Events</a>
				</li>
				<li class="nav-item">
					<a href="tips.php">Health tips</a>
				</li>
				<li id="droper" class="dropdown">
					<a href="login.php">
            		Login <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorlogin.php">Donor/Volentiers</a></li>
            		<li><a href="orglogin.php">Organizations</a></li>	
            		<li><a  href="userlogin.php">Users/Seekers</a></li>
            		<li><hr class="dropdown-divider"></li>

           			<li><a  href="adminlogin.php">Administrator</a></li>
          			</ul>
				</li>
				<li id="droper" class="dropdown">
					<a href="signup.php">
            		Sign Up! <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorsign.php">Donor/Volentiers</a></li>
            		<li><a href="orgsign.php">Organizations</a></li>	
            		<li><a  href="usersign.php">Users/Seekers</a></li>
            		</ul>
				</li>
				<li class="nav-item">
					<a href="request.php">Blood Request</a>
				</li>
				<li class="nav-item">
					<a href="">Search Donors</a>
				</li>
                <li class="nav-item">
					<a href="">About us</a>
				</li>
				</ul>
			</div>
		</div>
	</nav>

<div class="container-md">
	<div class="row">
		<div class="col-sm-4">

		</div>
			<div class="col-sm-4 " id="log" >
				<div >
      				<form  >
      					<div>
                         <div class="text-center">
                             
                       <Button class="btn">
                            User
                       </Button>
                       </div>
                       <div class="text-center" >
                       <Button class="btn">
                           Organization
                       </Button>
                       </div >
                       <div class="text-center">
                       <Button class="btn" >
                           Donor
                       </Button>
                       </div>
                       <li><hr class="dropdown-divider"></li>
                       <div  class="text-center" >
                       <Button class="btn">
                           Admin panel
                       </Button>
                       </div>
                        </div>
      				</form>
      			</div>
    		</div>
			<div class="col-sm-5"> 

			</div>

		</div>

	</div>
</div>
</body>
</html>