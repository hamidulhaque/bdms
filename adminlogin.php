<!DOCTYPE html>
<html oncontextmenu="return false">
<head >
    <title>Admin Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>

html {
  background-color: #56baed;
}

body {
  font-family: "Poppins", sans-serif;
  height: 100vh;
}

a {
  color: #92badd;
  display:inline-block;
  text-decoration: none;
  font-weight: 400;
}

h2 {
  text-align: center;
  font-size: 16px;
  font-weight: 600;
  text-transform: uppercase;
  display:inline-block;
  margin: 40px 8px 10px 8px; 
  color: #cccccc;
}



/* STRUCTURE */

.wrapper {
  display: flex;
  align-items: center;
  flex-direction: column; 
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
}

#formContent {
  -webkit-border-radius: 10px 10px 10px 10px;
  border-radius: 10px 10px 10px 10px;
  background-image: url("https://images.alphacoders.com/565/thumb-1920-565095.jpg");
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  padding: 30px;
  width: 90%;
  max-width: 450px;
  position: relative;
  padding: 0px;
  -webkit-box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  text-align: center;
}

#formFooter {
  background-color: #f6f6f6;
  border-top: 1px solid #dce8f1;
  padding: 25px;
  text-align: center;
  -webkit-border-radius: 0 0 10px 10px;
  border-radius: 0 0 10px 10px;
}



/* TABS */

h2.inactive {
  color: #cccccc;
}

h2.active {
  color: #0d0d0d;
  border-bottom: 2px solid #5fbae9;
}



/* FORM TYPOGRAPHY*/

input[type=button], input[type=submit], input[type=reset]  {
  background-color: #56baed;
  border: none;
  color: white;
  padding: 15px 80px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  text-transform: uppercase;
  font-size: 13px;
  -webkit-box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
  margin: 5px 20px 40px 20px;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out;
}

input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover  {
  background-color: #39ace7;
}

input[type=button]:active, input[type=submit]:active, input[type=reset]:active  {
  -moz-transform: scale(0.95);
  -webkit-transform: scale(0.95);
  -o-transform: scale(0.95);
  -ms-transform: scale(0.95);
  transform: scale(0.95);
}

input[type=text] {
  background-color: #f6f6f6;
  border: none;
  color: #0d0d0d;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 5px;
  width: 85%;
  border: 2px solid #f6f6f6;
  -webkit-transition: all 0.5s ease-in-out;
  -moz-transition: all 0.5s ease-in-out;
  -ms-transition: all 0.5s ease-in-out;
  -o-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
}

input[type=text]:focus {
  background-color: #fff;
  border-bottom: 2px solid #5fbae9;
}

input[type=text]:placeholder {
  color: #cccccc;
}



/* ANIMATIONS */

/* Simple CSS3 Fade-in-down Animation */
.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

@keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

/* Simple CSS3 Fade-in Animation */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fadeIn {
  opacity:0;
  -webkit-animation:fadeIn ease-in 1;
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fadeIn.first {
  
  -webkit-animation-delay: 0.4s;
  -moz-animation-delay: 0.4s;
  animation-delay: 0.4s;
}


.fadeIn.first h1 {
 color: #56baed;
 font-weight: bold;
 text-shadow: 2px 2px #ff0000;
 font-family: monospace;
}
.fadeIn.second {
  -webkit-animation-delay: 0.6s;
  -moz-animation-delay: 0.6s;
  animation-delay: 0.6s;
}

.fadeIn.third {
  -webkit-animation-delay: 0.8s;
  -moz-animation-delay: 0.8s;
  animation-delay: 0.8s;
}

.fadeIn.fourth {
  -webkit-animation-delay: 1s;
  -moz-animation-delay: 1s;
  animation-delay: 1s;
}

/* Simple CSS3 Fade-in Animation */
.underlineHover:after {
  display: block;
  left: 0;
  bottom: -10px;
  width: 0;
  height: 2px;
  background-color: #56baed;
  content: "";
  transition: width 0.2s;
}

.underlineHover:hover {
  color: #0d0d0d;
}

.underlineHover:hover:after{
  width: 100%;
}



/* OTHERS */

*:focus {
    outline: none;
} 

#icon {
  width:60%;
}


*{
	margin:0 ;
	padding: 0;
}
.header
{
	height: 100vh;
}
.body{
	font-family: monospace;
	outline: none;
	background-image: url('https://cdn.pixabay.com/photo/2021/09/19/19/04/blood-6638804_960_720.png');
	background-size: cover;
	background-repeat: no-repeat;
	height: 100vh;
}

.navbar-header a {
  position: relative;
  display: inline-block;
  font-size: 3em;
  font-weight: 800;
  color: royalblue;
  overflow: hidden;
  background: linear-gradient(to right, red, red 50%, lightgray 50%);
  background-clip: text;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  background-size: 200% 100%;
  background-position: 100%;
  transition: background-position 300ms ease;
  text-decoration: none; 
}
.navbar-header a:hover {
    background-position: 0 100%;
  }
#micon .navbar-nav li a
{
	color: darkgreen;
	font-size: 12px;
}

#micon #droper{
	transition: all o.2s ease-in-out  ;
}

#micon #dropers:hover{
	background-color: darkcyan;
}
.dropdown-menu{
	display: none;
	position: absolute;
	background-color: lightgreen;
	box-shadow: 2px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropdown:hover .dropdown-menu{
	display: block;
	transition: all o.3s ease-in-out;

}
.dropdown:hover .dropdown-menu a:hover{
	background-color: lightcoral;

}

.navbar-style
{
	box-shadow: 0 2px 5px 	#efefef;
	text-transform: uppercase;
}



.logo
{ 
	height: 48px;
	padding: 2px 10px;
}

.logo:hover {
  -webkit-transform: scaleX(-1);
  transform: scaleX(-1);
}
.icon-bar{
	background: darkcyan;
}


.divider:after,
.divider:before {
  content: "";
  flex: 1;
  height: 1px;
  background: #eee;
}
.h-custom {
  height: calc(100% - 73px);
}
@media (max-width: 450px) {
  .h-custom {
    height: 100%;
  }

}
</style>

</head>
<body>
	<header class="header">
	<nav class="navbar navbar-style">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#micon">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>	
				</button>


				<a href="main.php" ><img class="logo" src="logo.png" alt=""></img></a>
				<a href="main.php" >bdms</a>

			</div>

			<div class="collapse navbar-collapse" id="micon">
				<ul class="nav navbar-nav navbar-right">
				<li class="nav-item">
					<a href="main.php">Home</a>
				</li>
				<li class="nav-item">
					<a href="gallary.php">Gallery</a>
				</li>
				<li class="nav-item">
					<a href="events.php">Events</a>
				</li>
				<li class="nav-item">
					<a href="tips.php">Health tips</a>
				</li>
				<li id="droper" class="dropdown">
					<a href="login.php">
            		Login <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorlogin.php">Donor/Volentiers</a></li>
            		<li><a href="orglogin.php">Organizations</a></li>	
            		<li><a  href="userlogin.php">Users/Seekers</a></li>
            		<li><hr class="dropdown-divider"></li>

           			<li><a  href="adminlogin.php">Administrator</a></li>
          			</ul>
				</li>
				<li id="droper" class="dropdown">
					<a href="signup.php">
            		Sign Up! <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorsign.php">Donor/Volentiers</a></li>
            		<li><a href="orgsign.php">Organizations</a></li>	
            		<li><a  href="usersign.php">Users/Seekers</a></li>
            		</ul>
				</li>
				<li class="nav-item">
					<a href="request.php">Blood Request</a>
				</li>
				<li class="nav-item">
					<a href="">Search Donors</a>
				</li>
				<li class="nav-item">
					<a href="">About us</a>
				</li>
				</ul>
			</div>
		</div>
	</nav>


  <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <h1>Admin Panel</h1>
    </div>

    <!-- Login Form -->
    <form>
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="login" required>
      <input type="text" id="password" class="fadeIn third" name="login" placeholder="password" required>
           <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

  </div>
</div>
</body>
</html>