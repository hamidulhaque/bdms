<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
	<header class="header">
	<nav class="navbar navbar-style">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#micon">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>	
				</button>


				<a href="main.php" ><img class="logo" src="logo.png" alt=""></img></a>
				<a href="main.php" >bdms</a>

			</div>

			<div class="collapse navbar-collapse" id="micon">
				<ul class="nav navbar-nav navbar-right">
				<li class="nav-item">
					<a href="main.php">Home</a>
				</li>
				<li class="nav-item">
					<a href="gallary.php">Gallery</a>
				</li>
				<li class="nav-item">
					<a href="events.php">Events</a>
				</li>
				<li class="nav-item">
					<a href="tips.php">Health tips</a>
				</li>
				<li id="droper" class="dropdown">
					<a href="login.php">
            		Login <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorlogin.php">Donor/Volentiers</a></li>
            		<li><a href="orglogin.php">Organizations</a></li>	
            		<li><a  href="userlogin.php">Users/Seekers</a></li>
            		<li><hr class="dropdown-divider"></li>

           			<li><a  href="adminlogin.php">Administrator</a></li>
          			</ul>
				</li>
				<li id="droper" class="dropdown">
					<a href="signup.php">
            		Sign Up! <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorsign.php">Donor/Volentiers</a></li>
            		<li><a href="orgsign.php">Organizations</a></li>	
            		<li><a  href="usersign.php">Users/Seekers</a></li>
            		</ul>
				</li>
				<li class="nav-item">
					<a href="request.php">Blood Request</a>
				</li>
				<li class="nav-item">
					<a href="">Search Donors</a>
				</li>
				<li class="nav-item">
					<a href="">About us</a>
				</li>
				</ul>
			</div>
		</div>
	</nav>


	<div class="container">

		<div class="row align-items-start">
			<div class="col-sm-6"style="padding:10px">
				<h1>Blood Donation Management System</h1>
				<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<h1>Blood Donation Management System</h1>
				<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><h1>Blood Donation Management System</h1>
				<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			</div>
			
			<div class="col-sm-5" id="req" >
				<div >
      				
      				<form  >
      					<h1 align="center">Request For blood</h1>

      					<div class="mb-3"  >
      						
      						<input type="text" id="rname" placeholder="Your Name:" class="form-control" required>
      					</div>

      					<div class="mb-3" >
      						
      						<input type="email" id="remail" placeholder="Your Email:" class="form-control"required>
      					</div>

      					<div class="mb-3">
      						
      						<input type="text" id="rnumber" placeholder="Phone number:" class="form-control" required>
      						
      					</div>
      					
      					<div class="mb-3">
      					
      						<input type="text" id="rhname"  placeholder="Hostpital name:" class="form-control">
      						

      					</div>
      					<div class="mb-3">
      						<input type="text" id="rhflocation"  class="form-control" placeholder="Full Location">
      						

      					</div>

      					<div class="mb-3">

      						<label for="blgroup" id="rgroup">
      							Blood Group:
      						</label>
      						<select name="bloodgroup" id="rsgroup" class="form-select" required>
      						<option value="" selected>Select</option>
      						<option value="A+">A+</option>
      						<option value="B+">B+</option>
      						<option value="O+">O+</option>
      						<option value="AB+">AB+</option>
      						<option value="AB-">AB-</option>
      						<option value="A-">A-</option>
      						<option value="B-">B-</option>
      						<option value="O-">O-</option>
      						</select>
      						
      					</div>

      					<div class="mb-3">
							
      						<label class="form-label">
      						Blood Quantity:(Bags)
      						</label>
      						<input type="number" id="rbquantity"  class="form-control-inline" placeholder="ex: 1">
								
      					</div>
      					<div class="col-12 " >
    							<button  allign="center" class="btn btn-primary btn-block" type="submit">Submit form</button>
  						</div>
      				</form>
      			</div>
    		</div>
		</div>
		
		
	</div>


	<!-- Footer Section -->
	
</body>
</html>
