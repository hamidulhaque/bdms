<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<header class="header">
	<nav class="navbar navbar-style">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#micon">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>	
				</button>


				<a href="main.php" ><img class="logo" src="logo.png" alt=""></img></a>
				<a href="main.php" >bdms</a>

			</div>

			<div class="collapse navbar-collapse" id="micon">
				<ul class="nav navbar-nav navbar-right">
				<li class="nav-item">
					<a href="main.php">Home</a>
				</li>
				<li class="nav-item">
					<a href="gallary.php">Gallery</a>
				</li>
				<li class="nav-item">
					<a href="events.php">Events</a>
				</li>
				<li class="nav-item">
					<a href="tips.php">Health tips</a>
				</li>
				<li id="droper" class="dropdown">
					<a href="login.php">
            		Login <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorlogin.php">Donor/Volentiers</a></li>
            		<li><a href="orglogin.php">Organizations</a></li>	
            		<li><a  href="userlogin.php">Users/Seekers</a></li>
            		<li><hr class="dropdown-divider"></li>

           			<li><a  href="adminlogin.php">Administrator</a></li>
          			</ul>
				</li>
				<li id="droper" class="dropdown">
					<a href="signup.php">
            		Sign Up! <span class="caret"></span>
          			</a>
          			<ul class="dropdown-menu">
            		<li><a  href="donorsign.php">Donor/Volentiers</a></li>
            		<li><a href="orgsign.php">Organizations</a></li>	
            		<li><a  href="usersign.php">Users/Seekers</a></li>
            		</ul>
				</li>
				<li class="nav-item">
					<a href="request.php">Blood Request</a>
				</li>
				<li class="nav-item">
					<a href="">Search Donors</a>
				</li>
				<li class="nav-item">
					<a href="">About us</a>
				</li>
				</ul>
			</div>
		</div>
	</nav>
</body>
</html>